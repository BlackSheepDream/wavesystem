﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Countdown
{
    [SerializeField]
    private float time;
    [SerializeField]
    private bool isFinished;
    [ReadOnly]
    private float currentTime;

    public bool IsFinished
    {
        get
        {
            return isFinished;
        }
    }

    public float CurrentTime
    {
        get
        {
            return currentTime;
        }
    }

    public void Start()
    {
        Reset(time);
    }

    public void Update(float delta)
    {
        if (!isFinished)
        {
            currentTime -= delta;

            if (currentTime <= 0)
                isFinished = true;
        }
    }

    public void Reset()
    {
        Reset(time);
    }

    public void Reset(float time)
    {
        currentTime = time;
        isFinished = false;
    }
}
