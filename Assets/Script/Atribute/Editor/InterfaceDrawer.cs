﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;

[CustomPropertyDrawer(typeof(InterfaceAttribute))]
public class InterfaceDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        InterfaceAttribute interfaceAttribute = attribute as InterfaceAttribute;

        Type type = interfaceAttribute.Type;

        if (!type.IsInterface)
            EditorGUI.LabelField(position, label.text, "Type is not an interface");
        else if (property.propertyType == SerializedPropertyType.ObjectReference)
        {
            label.text += " (" + type.Name + ")";
            var mono = EditorGUI.ObjectField(position, label, property.objectReferenceValue, typeof(MonoBehaviour), true);

            if (mono != null && type.IsAssignableFrom(mono.GetType()))
                property.objectReferenceValue = mono;
            else if(mono == null)
                property.objectReferenceValue = null;
        }
        else
            EditorGUI.LabelField(position, label.text, "Use Interface with Object Reference (MonoBehaviour).");
    }
}
