﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var rect = EditorGUI.PrefixLabel(position, label);
        
        switch (property.propertyType)
        {
            case SerializedPropertyType.Integer:
                EditorGUI.LabelField(rect, property.intValue.ToString());
                break;
            case SerializedPropertyType.Float:
                EditorGUI.LabelField(rect, property.floatValue.ToString());
                break;
            case SerializedPropertyType.Boolean:
                EditorGUI.LabelField(rect, property.boolValue.ToString());
                break;
            case SerializedPropertyType.String:
                EditorGUI.LabelField(rect, property.stringValue);
                break;
        }
    }
}
