﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceAttribute : PropertyAttribute
{
    public Type Type { get; }

    public InterfaceAttribute(Type type)
    {
        Type = type;
    }
}
