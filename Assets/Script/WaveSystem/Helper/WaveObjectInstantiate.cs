﻿using UnityEngine;

namespace RaposaDoArtico.WaveSystem.Helper
{
    public class WaveObjectInstantiate : WaveObjectManager
    {
        [SerializeField]
        private WaveObject Prefab;

        public override WaveObject GetWaveObject()
        {
            return Instantiate(Prefab);
        }
    }
}