﻿using UnityEngine;

namespace RaposaDoArtico.WaveSystem.Helper
{
    public abstract class WaveObjectManager : MonoBehaviour
    {
        public abstract WaveObject GetWaveObject();
    }
}