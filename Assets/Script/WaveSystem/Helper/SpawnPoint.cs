﻿using UnityEngine;

namespace RaposaDoArtico.WaveSystem.Helper
{
    public class SpawnPoint : MonoBehaviour
    {
        [SerializeField]
        private Transform target;

        public Transform GetPoint()
        {
            return target;
        }
    }
}