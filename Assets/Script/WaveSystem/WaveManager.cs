﻿using RaposaDoArtico.WaveSystem.Wave;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RaposaDoArtico.WaveSystem
{
    public class WaveManager : MonoBehaviour, IWaveObserver
    {
        [SerializeField]
        private int currentIndexWave;

        public WaveSystemMode mode;

        [Header("Config")]
        public float delayBetweenWave;

        [Header("Event")]
        public UnityEvent OnWaveStart;
        public UnityEvent OnWaveComplete;
        public PrimitiveEvent.IntEvent OnChangedWave;
        public PrimitiveEvent.FloatEvent OnCountdownNextWave;

        [Header("Wave List")]
        [SerializeField]
        private WaveBehaviour[] waves;

        private WaveBehaviour currentWave;

        public static WaveManager Instance
        {
            get;
            private set;
        }

        public bool IsCurrentWaveIsCompleted
        {
            get { return (currentWave != null && currentWave.IsCompleted) || currentWave == null; }
        }

        public bool IsHasNextWave
        {
            get
            {
                return currentIndexWave + 1 < waves.Length;
            }
        }

        public WaveBehaviour CurrentWave
        {
            get
            {
                return currentWave;
            }
        }

        private void OnValidate()
        {
            currentIndexWave = Mathf.Min(Mathf.Max(currentIndexWave, 0), waves.Length);
        }

        private void Start()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        public void StartOrPlayNextWave()
        {
            if (currentWave == null && currentIndexWave < waves.Length)
            {
                currentWave = waves[currentIndexWave];
            }

            if (currentWave != null && !currentWave.IsInitializing)
            {
                StartCurrentWave();
            }
            else
            {
                NextWave();
            }
        }

        public void StartWave(int waveNumber)
        {
            if (!IsCurrentWaveIsCompleted)
            {
                return;
            }

            currentWave = waves[waveNumber];
            StartCurrentWave();
        }

        public void StartCurrentWave()
        {
            if (currentWave == null)
            {
                Debug.LogWarning("Current wave reference is null", currentWave);
                return;
            }

            OnWaveStart.Invoke();
            currentWave.Begin(this);
        }

        public void NextWave()
        {
            if (IsCurrentWaveIsCompleted)
            {
                if (IsHasNextWave)
                {
                    currentIndexWave++;
                    OnChangedWave.Invoke(currentIndexWave);
                    StartWave(currentIndexWave);
                }
                else
                {
                    LevelCompleted();
                }
            }
        }

        public void OnWaveCompleted()
        {
            OnWaveComplete.Invoke();

            if (mode == WaveSystemMode.Automatic)
            {
                if (IsHasNextWave)
                {
                    StartCoroutine(DelayCallback(delayBetweenWave, NextWave));
                }
                else
                {
                    LevelCompleted();
                }
            }
        }

        public void LevelCompleted()
        {
            Debug.Log("Level Completed");
        }

        private IEnumerator DelayCallback(float delay, UnityAction action)
        {
            while (delay > 0)
            {
                delay -= Time.deltaTime;
                OnCountdownNextWave.Invoke(delay);
                yield return null;
            }

            action.Invoke();
        }

        [ContextMenu("Search Waves In Child")]
        private void SearchWavesInChild()
        {
            List<WaveBehaviour> waves = new List<WaveBehaviour>();

            foreach (Transform child in transform)
            {
                WaveBehaviour wave = child.GetComponent<WaveBehaviour>();
                if (wave != null)
                {
                    waves.Add(wave);
                }
            }

            this.waves = waves.ToArray();
        }

        public enum WaveSystemMode
        {
            Automatic, Manual
        }
    }
}
