﻿using UnityEngine;

namespace RaposaDoArtico.WaveSystem.Wave
{
    public abstract class WaveBehaviour : MonoBehaviour, IWaveObjectObserver, IWaveObserver
    {
        [Header("Info")]
        [ReadOnly]
        [SerializeField]
        private bool isInitializing;
        [ReadOnly]
        [SerializeField]
        private bool isCompletedSpawn;
        [ReadOnly]
        [SerializeField]
        private bool isCompleted;

        [SerializeField]
        [HideInInspector]
        private bool isExecuteWhenCompleted;

        [Header("Extended")]
        [HideInInspector]
        [SerializeField]
        private WaveBehaviour waveExtended;

        private IWaveObserver waveObserver;

        public bool IsCompleted
        {
            get
            {
                return isCompleted;
            }
        }

        public bool IsCompletedSpawn
        {
            get
            {
                return isCompletedSpawn;
            }
        }

        public bool IsInitializing
        {
            get
            {
                return isInitializing;
            }
        }

        public void Begin(IWaveObserver waveObserver)
        {
            this.waveObserver = waveObserver;
            isInitializing = true;
            OnBegin();
        }

        protected abstract void OnBegin();

        public abstract void OnWaveObjectDisable(WaveObject waveObject);

        protected void CompletedSpawn()
        {
            if (waveExtended != null && !isExecuteWhenCompleted)
            {
                waveExtended.Begin(this);
            }

            isCompletedSpawn = true;
        }

        protected void Finished()
        {
            isCompleted = true;
            WaveCompleted();
        }

        public void OnWaveCompleted()
        {
            WaveCompleted();
        }

        public void WaveCompleted()
        {
            if (isExecuteWhenCompleted)
            {
                waveExtended.Begin(this);
            }

            if ((waveExtended != null && waveExtended.isCompleted && isCompleted) || (waveExtended == null && isCompleted))
            {
                waveObserver.OnWaveCompleted();
            }
        }

        [ContextMenu("Auto Config")]
        public void AutoConfig()
        {
            var waves = gameObject.GetComponents<WaveBehaviour>();

            for (int i = 0; i < waves.Length - 1; i++)
                waves[i].waveExtended = waves[i + 1];

            waves[waves.Length - 1].waveExtended = null;
        }
    }
}