﻿using RaposaDoArtico.WaveSystem.Helper;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace RaposaDoArtico.WaveSystem.Wave
{
    public class WaveRandom : WaveBehaviour
    {
        [Header("Config WaveObject")]
        public WaveObjectConfig[] waveObjectConfigs;

        [Header("Spawn Option")]
        [SerializeField]
        private float minDelaySpawn;
        [SerializeField]
        private float maxDelaySpawn;
        [SerializeField]
        private SpawnPoint spawnPoint;

        private int currentWaveObjectDisableAmount;

        private int CurrentSpawnAmount
        {
            get
            {
                int total = 0;

                foreach (var config in waveObjectConfigs)
                    total += config.currentSpawnAmount;

                return total;
            }
        }

        private List<WaveObjectConfig> objectConfigs;
        private Countdown countdown;

        protected override void OnBegin()
        {
            countdown = new Countdown();
            objectConfigs = waveObjectConfigs.ToList();

            enabled = true;
            countdown.Reset(DelaySpawn());
        }

        private void Update()
        {
            if (!countdown.IsFinished)
            {
                countdown.Update(Time.deltaTime);
                return;
            }

            Spawn();

            if (objectConfigs.Count == 0)
            {
                CompletedSpawn();
                enabled = false;
            }
            else
                countdown.Reset(DelaySpawn());
        }

        public override void OnWaveObjectDisable(WaveObject waveObject)
        {
            currentWaveObjectDisableAmount++;

            if (currentWaveObjectDisableAmount == CurrentSpawnAmount)
            {
                Finished();
            }
        }

        private void Spawn()
        {
            var waveObject = GetWaveObject();

            waveObject.gameObject.SetActive(true);
            waveObject.waveObserver = this;
            waveObject.SetLocation(spawnPoint.GetPoint());
        }

        private WaveObject GetWaveObject()
        {
            var waveObjectConfig = RandomWaveObjectConfig();
            var waveObject = waveObjectConfig.WaveObjectManager.GetWaveObject();

            waveObjectConfig.currentSpawnAmount++;

            if (waveObjectConfig.IsCompleted)
                objectConfigs.Remove(waveObjectConfig);

            return waveObject;
        }

        private WaveObjectConfig RandomWaveObjectConfig()
        {
            return objectConfigs[Random.Range(0, objectConfigs.Count)];
        }

        private float DelaySpawn()
        {
            return Random.Range(minDelaySpawn, maxDelaySpawn);
        }

        [System.Serializable]
        public class WaveObjectConfig
        {
            public WaveObjectManager WaveObjectManager;
            public int spawnAmount;
            public int currentSpawnAmount;

            public bool IsCompleted
            {
                get
                {
                    return spawnAmount <= currentSpawnAmount;
                }
            }
        }
    }
}