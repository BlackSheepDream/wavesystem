﻿using RaposaDoArtico.WaveSystem.Helper;
using UnityEngine;

namespace RaposaDoArtico.WaveSystem.Wave
{
    public class WaveSequence : WaveBehaviour
    {
        [SerializeField]
        private WaveObjectManager objectManager;

        [ReadOnly]
        [SerializeField]
        private int currentSpawnAmount;
        [ReadOnly]
        [SerializeField]
        private int currentWaveObjectDisableAmount;

        [Header("Spawn Option")]
        [SerializeField]
        private int spawnAmount;
        [SerializeField]
        private float minDelaySpawn;
        [SerializeField]
        private float maxDelaySpawn;
        [SerializeField]
        private SpawnPoint spawnPoint;

        private Countdown countdown;

        public float MinDelaySpawn
        {
            get
            {
                return minDelaySpawn;
            }

            set
            {
                minDelaySpawn = Mathf.Abs(Mathf.Min(value, MaxDelaySpawn));
            }
        }

        public float MaxDelaySpawn
        {
            get
            {
                return maxDelaySpawn;
            }

            set
            {
                maxDelaySpawn = Mathf.Abs(Mathf.Max(MinDelaySpawn, value));
            }
        }

        public int SpawnAmount
        {
            get
            {
                return spawnAmount;
            }

            set
            {
                spawnAmount = value;
            }
        }

        #region MonoBehaviour
        private void Awake()
        {
            enabled = false;
        }

        private void OnValidate()
        {
            minDelaySpawn = Mathf.Abs(Mathf.Min(MinDelaySpawn, MaxDelaySpawn));
            maxDelaySpawn = Mathf.Abs(Mathf.Max(MinDelaySpawn, MaxDelaySpawn));
        }

        private void Update()
        {
            if (!countdown.IsFinished)
            {
                countdown.Update(Time.deltaTime);
                return;
            }

            Spawn();

            if (currentSpawnAmount < spawnAmount)
            {
                countdown.Reset(DelaySpawn());
            }
            else
            {
                CompletedSpawn();
                enabled = false;
            }
        }
        #endregion

        protected override void OnBegin()
        {
            countdown = new Countdown();
            countdown.Reset(DelaySpawn());
            enabled = true;
        }

        public override void OnWaveObjectDisable(WaveObject waveObject)
        {
            currentWaveObjectDisableAmount++;

            if (currentWaveObjectDisableAmount == currentSpawnAmount)
            {
                Finished();
            }
        }

        private void Spawn()
        {
            var waveObject = GetWaveObject();
            var point = spawnPoint.GetPoint();

            waveObject.gameObject.SetActive(true);
            waveObject.SetLocation(point);

            currentSpawnAmount++;
        }

        private float DelaySpawn()
        {
            return Random.Range(minDelaySpawn, maxDelaySpawn);
        }

        private WaveObject GetWaveObject()
        {
            var waveObject = objectManager.GetWaveObject();
            waveObject.waveObserver = this;
            return waveObject;
        }
    }
}