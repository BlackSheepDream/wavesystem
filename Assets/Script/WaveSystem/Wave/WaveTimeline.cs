﻿using UnityEngine.Playables;
using UnityEngine;

namespace RaposaDoArtico.WaveSystem.Wave
{
    public class WaveTimeline : WaveBehaviour
    {
        [Header("Depedency")]
        [SerializeField]
        private PlayableDirector playableDirector;

        protected override void OnBegin()
        {
            playableDirector.Play();
            playableDirector.stopped += OnStopped;
        }

        private void OnStopped(PlayableDirector obj)
        {
            CompletedSpawn();
            Finished();
        }

        public override void OnWaveObjectDisable(WaveObject waveObject)
        {
        }
    }
}