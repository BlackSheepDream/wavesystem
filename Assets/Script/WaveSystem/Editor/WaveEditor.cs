﻿using RaposaDoArtico.WaveSystem.Wave;
using UnityEditor;
using UnityEngine;

namespace RaposaDoArtico.WaveSystem.Editor
{
    [CustomEditor(typeof(WaveBehaviour), true)]
    public class WaveEditor : UnityEditor.Editor
    {
        private WaveBehaviour script;

        private void OnEnable()
        {
            script = target as WaveBehaviour;
        }

        public override void OnInspectorGUI()
        {
            GuidDrawLabel(script);
            base.OnInspectorGUI();
            ExtendedDraw();
        }

        public void GuidDrawLabel(Object script)
        {
            GuidDrawLabel(script, "InstanceID");
        }

        public void GuidDrawLabel(Object script, string prefixLabel)
        {
            EditorGUILayout.BeginHorizontal();
            EditorStyles.label.fontStyle = FontStyle.Bold;
            EditorGUILayout.PrefixLabel(prefixLabel);
            EditorGUILayout.LabelField(script.GetInstanceID().ToString());
            EditorStyles.label.fontStyle = FontStyle.Normal;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
        }

        public void ExtendedDraw()
        {
            var property = serializedObject.FindProperty("waveExtended");
            EditorGUILayout.PropertyField(property);

            WaveBehaviour waveTarget = property.objectReferenceValue as WaveBehaviour;
            
            if(waveTarget == null)
                serializedObject.ApplyModifiedProperties();
            else if (waveTarget.GetInstanceID() != script.GetInstanceID())
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("isExecuteWhenCompleted"));
                serializedObject.ApplyModifiedProperties();
                GuidDrawLabel(waveTarget, "Wave Extended InstanceID");
            }
        }
    }
}