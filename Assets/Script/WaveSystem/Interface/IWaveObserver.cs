﻿namespace RaposaDoArtico.WaveSystem
{
    public interface IWaveObserver
    {
        void OnWaveCompleted();
    }
}