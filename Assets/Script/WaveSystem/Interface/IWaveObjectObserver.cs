﻿namespace RaposaDoArtico.WaveSystem
{
    public interface IWaveObjectObserver
    {
        void OnWaveObjectDisable(WaveObject waveObject);
    }
}