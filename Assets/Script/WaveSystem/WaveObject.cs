﻿using UnityEngine;
using UnityEngine.Events;

namespace RaposaDoArtico.WaveSystem
{
    public class WaveObject : MonoBehaviour
    {
        [Header("Event")]
        [Tooltip("Object when spawned by Wave, notify event will be triggered")]
        public UnityEvent OnNotify;

        public IWaveObjectObserver waveObserver;

        public void SetLocation(Transform point)
        {
            SetLocation(point.position, point.rotation);
        }

        public void SetLocation(Vector3 position)
        {
            SetLocation(position, Quaternion.identity);
        }

        public void SetLocation(Vector3 position, Quaternion rotation)
        {
            transform.position = position;
            transform.rotation = rotation;
        }

        private void OnDisable()
        {
            RemoveWaveObserver();
        }

        private void OnDestroy()
        {
            RemoveWaveObserver();
        }

        public void RemoveWaveObserver()
        {
            if (waveObserver != null)
            {
                waveObserver.OnWaveObjectDisable(this);
                waveObserver = null;
            }
        }
    }
}